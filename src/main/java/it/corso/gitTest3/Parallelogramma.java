package it.corso.gitTest3;

public class Parallelogramma {
	
	private int latoMaggiore;
	private int latoMinore;
	private int altezza;
	private int area;
	
	public Parallelogramma(int newlatoMaggiore, int newlatoMinore, int newaltezza, int newarea){
		this.latoMaggiore = newlatoMaggiore;
		this.latoMinore = newlatoMinore;
		this.altezza = newaltezza;
		this.area = newarea;
	}
	
	public int calcolaDiagonale(){
		int diagonale = this.area/this.latoMaggiore;
		return diagonale;
	}
	
	
	public int calcolaPerimetro(){
		int perimetro = this.latoMaggiore * this.latoMaggiore + this.calcolaDiagonale() * this.calcolaDiagonale();
		return perimetro;
	}
	
	public String calcolaTipologia(){
		String tipo = "";
	}

}
