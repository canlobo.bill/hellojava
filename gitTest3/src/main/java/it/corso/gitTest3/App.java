package it.corso.gitTest3;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        Parallelogramma uno = new Parallelogramma(10,10,5,20);
        System.out.println(uno.calcolaDiagonale());
        System.out.println(uno.calcolaPerimetro());
    }
}
