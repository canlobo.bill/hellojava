# HelloJava

## Descrizione
Prima esercitazione per prendere confidenza con java

## Traccia

Definisci una classe Parallelogramma con i seguenti attributi: diagonale maggiore  diagonale minore e angolo che la diagonale maggiore forma rispetto l'orizzontale.
Deve essere possibile calcolare:
1. I lati
2. L'area
3. Perimetro del parallelogramma
4. Tipologia di parallelogramma (quadrato, rombo, etc) 

**IMPORTANTE** non dimenticare l' uso dei **package**  e **SRP** (single responsibility principle)